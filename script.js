/*
У файлі index.html лежить розмітка двох полів вводу пароля.
Після натискання на іконку поруч із конкретним полем - повинні відображатися
 символи, які ввів користувач, іконка змінює свій зовнішній вигляд.
 
 У коментарях під іконкою - інша іконка, саме вона повинна відображатися
  замість поточної.

Коли пароля не видно - іконка поля має виглядати як та, що в першому
 полі (Ввести пароль)

Коли натиснута іконка, вона має виглядати, як та, що у другому
 полі (Ввести пароль)

Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях

Якщо значення збігаються – вивести модальне вікно (можна alert) з
 текстом – You are welcome;

Якщо значення не збігаються - вивести під другим полем текст червоного
 кольору Потрібно ввести однакові значення


Після натискання на кнопку сторінка не повинна перезавантажуватись
*/



console.log("is working");

const icons = document.querySelectorAll(".fas")
const inputPasswords = document.querySelectorAll(".input-wrapper>input")
const btnConfirm = document.querySelector(".btn")
const errorText = document.createElement("p")


icons.forEach((icon, i) => {
    icon.addEventListener("click", () => {
        if (inputPasswords[i].type === "password") {
            inputPasswords[i].type = "text"
            icon.classList.toggle("icon-password")
        } else {
            inputPasswords[i].type = "password"
            icon.classList.toggle("icon-password")
        }
    })
});

btnConfirm.addEventListener("click", (e) => {
    e.preventDefault()   
    if (inputPasswords[0].value && (inputPasswords[0].value === inputPasswords[1].value)) {        
        alert("You are welcome")
    }else {        
        errorText.innerText = "Потрібно ввести однакові значення"      
        errorText.classList.add("error-text")
        inputLabels[1].after(errorText)
    }    
})

inputPasswords.forEach(input => {
    input.addEventListener("input", ()=> {
        errorText.remove()
    })
});




const inputLabels = document.querySelectorAll(".input-wrapper")
// inputLabels.forEach((el, i) => {
//     el.addEventListener("mousedown", (e) => {
//         if (e.target.matches("i")) {
//             inputPasswords[i].type = "text"
//         }
//     })
//     el.addEventListener("mouseup", (e) => {
//         if (e.target.matches("i")) {
//             inputPasswords[i].type = "password"
//         }
//     })

// });

